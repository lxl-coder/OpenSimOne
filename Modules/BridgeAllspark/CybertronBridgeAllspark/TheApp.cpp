#include "TheApp.hpp"
//#include "cybertron/core/UtilUrlRequest.hpp"
#include "cybertron/core/Log.hpp"
#include "cybertron/core/UtilPath.hpp"
#include "cybertron/core/Thread.hpp"
#include "cybertron/core/UtilConsole.hpp"
#include "cybertron/core/UtilCRT.hpp"
#include "cybertron/node/StartupArguments.hpp"
#include "cybertron/core/CoreVersion.hpp"
#include "cybertron/core/SimOneEnv.hpp"
#include "cybertron/core/UtilUuidxx.hpp"
#include "cybertron/glm/quat.hpp"
#include "cybertron/glm/mat4.hpp"
#include "Sensor/Vehicle.pb.h"
#include "Sensor/SensorCommon.pb.h"
#include <iostream>
#include <fstream>
#include <chrono>
#ifdef CYBERTRON_LINUX
#include <unistd.h>
#include <stdlib.h>  
#include <signal.h>
#endif
// determined by the EnvVar LOG_CONSOLE outside on the windows
//#if defined(CYBERTRON_WIN) && defined(NDEBUG)
//#pragma comment(linker, "/subsystem:\"windows\" /entry:\"mainCRTStartup\"" )
//#endif

using namespace cybertron;

//
static UtilCRTAutoInitialize sCRT;

#ifdef CYBERTRON_LINUX
bool gRunning = true;
void sigterm_handler(int no) {
	gRunning = false;
	logInfo("recv sigterm_handler exit....%d", no);
}
#endif

TheApp::TheApp()
{
	mbRunning = true;
	mBridgeAllsparkListenPort = 28237;
}
TheApp::~TheApp()
{
	
}


//BaseApp
//////////////////////////////////////
void TheApp::getVerion()
{
	std::string version = getVersionOnCore("CybertronWork", "20210626154200");
	logInfo("%s", version.c_str());
}



int TheApp::run()
{
	UtilCRT::call("runDaemon", [this]() {
		this->runDaemon();
	});

	return 0;
}

void TheApp::runDaemon(){

	//while (true)
	//{
	//	std::this_thread::sleep_for(std::chrono::seconds(1));
	//	Common::EWorkNodeState s = mWorkState.load(memory_order_relaxed);
	//	if (s == Common::EWorkNodeStateEndingWork) {
	//		logInfo("################1work stat = %d", s);
	//		return;
	//	}
	//	else if (s >= Common::EWorkNodeStateInitFinish) {
	//		logInfo("################2work stat = %d", s);
	//		break ;
	//	}
	//} 

	while(mbRunning){
#ifdef CYBERTRON_LINUX
	signal(SIGTERM, sigterm_handler);
#endif
	// std::this_thread::sleep_for(std::chrono::milliseconds(100));
	/*if (!isRunning()){
		break;
	}*/
	WorkAppWithHotArea::handleTimer();
	}
	stopRunning();
}

//WorkApp
///////////////////////////////////////////////////
void TheApp::onWorkNodeStop(){
	logInfo("my test work node is stop.");
	stopRunning();
}
void TheApp::testThread() {
	/*int count = 0;
	while (true)
	{
		if (count > 5) {
			
			
			break;
		}
		count += 1;
		logInfo("testThread==========>%d", count);
		std::this_thread::sleep_for(std::chrono::milliseconds(1000));
	}

	
	while (true)
	{
		sendGetWorkDynamicConfig("", Common::EWorkNodeType_CybertronNodeTimer);
		Common::SyncWorkNodeDynamicConfig config;
		bool ret = getWorkDynamicConfig(Common::EWorkNodeType_CybertronNodeTimer, &config);
		if (!ret) {
			std::this_thread::sleep_for(std::chrono::milliseconds(1000));
			continue;
		}
		logInfo("Common::SyncWorkNodeDynamicConfig ====>%d %s:%d", config.taskid().c_str(),config.workpublicip().c_str(),config.workpublicport());
		break;
		
	}*/
	return;
}

bool TheApp::onLoadConfig() {
	if (!WorkAppWithHotArea::onLoadConfig()) {
		return false;
	}
	return true;
}
bool TheApp::onConnectNodeTimerAfter() {
	if (!WorkAppWithHotArea::onConnectNodeTimerAfter()) {
		return false;
	}
	return true;
}

bool TheApp::onTaskStart() {
	logInfo("onTaskStart");
	connectNodeTimer();

	std::vector<std::string> *workIdList = getWorkNodeDependHotAreaId();
	for (auto item = workIdList->begin(); item != workIdList->end(); item++) 
	{
		mWorkID = getWorkIdByHostVehicleId(*item);
		mVehicleId = *item;
		if (!(connectWorkServer(mWorkID, mDynClient)))
		{
			logError("Connect work server failed");
			return false;
		}
	}

	std::thread serverRecvThread(&TheApp::StartTCPMultiPointServer, this);
	serverRecvThread.detach();

	HotArea::MainVehicleExtraDataIndics indics;
	indics.set_vehicleid(atoi(mVehicleId.c_str()));
	for (int i=0; i<=109; i++)
	{
		indics.add_extra_state_indics((ESimOne_Data_Vehicle_State)i);
	}
	Message msg;
	msg.fromProtobuf(cybertron::proto::sensor::EDataType_VehicleExtraStateIndics, indics);
	mDynClient.send(msg);

	return true;
}

bool TheApp::onTaskRunning() {
	logInfo("onTaskRunning");
	//mHotAreaManager.setTaskRunning("1234");
	return true;
}

bool TheApp::onTaskStop() { 
	logInfo("onTaskStop");
	mbRunning = false;
	return true;
}



//WorkAppWithHotArea
/////////////////////////////////////
bool TheApp::onStart(){
	logInfo("WorkNodeWithHotarea test, on start");
	return true;
}

void TheApp::onTimer(int framestamp, int ms){
	const std::vector<std::shared_ptr<TrafficActorHotArea>> &hotAreas = mHotAreaManager.getHotAreas();
	for (const auto& pHotArea : hotAreas) 
	{
		for (const auto& actorId : pHotArea->getActorIdSet()) 
		{
			const TrafficActor &actor = pHotArea->getActor(actorId);
			if (actor.getActorType() != HotArea::EActorType_MainVehicle)
			{
				continue;
			}
			if (actorId != atoi(mVehicleId.c_str()))
			{
				continue;
			}
			vec3 pos = actor.getWorldPosition();
			vec3 rot = actor.getWorldRotation();

			if (mOdometer < -1e-6) 
			{
				mOdometer = 0;
			}
			else 
			{
				mOdometer += cybertron::length(pos - mLastPos);
			}
			mLastPos = pos;

			HotArea::MainVehiclePrivateData MainVehiclePrivateData;

			if (actor.getCustomProperties(MainVehiclePrivateData))
			{
				mGpsInfo.engineRpm = MainVehiclePrivateData.rpm();
				mGpsInfo.steering = MainVehiclePrivateData.steering();
				mGpsInfo.throttle = MainVehiclePrivateData.throttle();
				mGpsInfo.brake = MainVehiclePrivateData.brake();
				mGpsInfo.steering = MainVehiclePrivateData.steering();
				mGpsInfo.gear = MainVehiclePrivateData.gear();
				mGpsInfo.angVelX = MainVehiclePrivateData.angular_velocity_x();
				mGpsInfo.angVelY = MainVehiclePrivateData.angular_velocity_y();
				mGpsInfo.angVelZ = MainVehiclePrivateData.angular_velocity_z();
				mGpsInfo.wheelSpeedFL = MainVehiclePrivateData.wheel_speed_fl();
				mGpsInfo.wheelSpeedFR = MainVehiclePrivateData.wheel_speed_fr();
				mGpsInfo.wheelSpeedRL = MainVehiclePrivateData.wheel_speed_rl();
				mGpsInfo.wheelSpeedRR = MainVehiclePrivateData.wheel_speed_rr();

				vec3 Vel;
				Vel.x = MainVehiclePrivateData.linear_velocity_x();
				Vel.y = MainVehiclePrivateData.linear_velocity_y();
				Vel.z = MainVehiclePrivateData.linear_velocity_z();


				vec3 Accel;
				Accel.x = MainVehiclePrivateData.linear_acceleration_x();
				Accel.y = MainVehiclePrivateData.linear_acceleration_y();
				Accel.z = MainVehiclePrivateData.linear_acceleration_z();

				vec3 AngVel;
				AngVel.x = MainVehiclePrivateData.angular_velocity_x();
				AngVel.y = MainVehiclePrivateData.angular_velocity_y();
				AngVel.z = MainVehiclePrivateData.angular_velocity_z();

				vec3 localAcc = inverse(mat4_cast(rot)) * Accel;

				mGpsInfo.posX = pos.x;
				mGpsInfo.posY = pos.y;
				mGpsInfo.posZ = pos.z;

				mGpsInfo.oriX = rot.x;
				mGpsInfo.oriY = rot.y;
				mGpsInfo.oriZ = rot.z;

				mGpsInfo.velX = Vel.x;
				mGpsInfo.velY = Vel.y;
				mGpsInfo.velZ = Vel.z;

				mGpsInfo.accelX = localAcc.x;
				mGpsInfo.accelY = localAcc.y;
				mGpsInfo.accelZ = localAcc.z;

				mGpsInfo.angVelX = AngVel.x;
				mGpsInfo.angVelY = AngVel.y;
				mGpsInfo.angVelZ = AngVel.z;

				mGpsInfo.odometer = mOdometer;

				mGpsInfo.extraStateSize = MainVehiclePrivateData.extrastates_size();
				for (int i = 0; i < MainVehiclePrivateData.extrastates_size(); i++) 
				{
					mGpsInfo.extraStates[i] = (MainVehiclePrivateData.extrastates(i));
				}

				Bridge2AllsparkStruct bridge2AllsparkData;
				bridge2AllsparkData.vehData = mGpsInfo;

				/*cout 
					<< clock() << " "
					<< bridge2AllsparkData.vehData.posX << " "
					<< bridge2AllsparkData.vehData.posY << " "
					<< bridge2AllsparkData.vehData.posZ << " "
					<< bridge2AllsparkData.vehData.extraStates[109] << " "
					<< framestamp << " "
					<< ms << " "
					<< endl;*/

				for (int i=0; i<mConnNodeSocket.size(); i++)
				{
					send(mConnNodeSocket[i], (char *)&bridge2AllsparkData, sizeof(bridge2AllsparkData), 0);
				}
			}
		}
	}
}

void TheApp::onTimer(int framestamp, int ms, int msModNs){
	// logInfo("WorkNodeWithHotarea test, on Timer2");
}

void TheApp::onPlay(){
	logInfo("WorkNodeWithHotarea test, on Play");
}

void TheApp::onPause(){
	logInfo("WorkNodeWithHotarea test, on Pause");
}

void TheApp::onReset(){
	logInfo("WorkNodeWithHotarea test, on Reset");
}

std::string TheApp::onControlCommand(const std::string controlCommand){
	logInfo("WorkNodeWithHotarea test, on Control Command: %s", controlCommand.c_str());
	return controlCommand;
}

void TheApp::onEnd(){
	logInfo("WorkNodeWithHotarea test, on End");
}

void TheApp::onEnd(Common::ETaskEndReason reason){
	logInfo("WorkNodeWithHotarea test, on End with reason");
}

void TheApp::onCaseStart(){
	logInfo("WorkNodeWithHotarea test, on CaseStart");
}

void TheApp::onCaseStop(){
	logInfo("WorkNodeWithHotarea test, on CaseStop");
}

void TheApp::StartTCPMultiPointServer()
{
	int listenPort = mBridgeAllsparkListenPort;
	WORD wVersionRequested;//版本号
	WSADATA wsaData;
	int err;

	wVersionRequested = MAKEWORD(1, 1);//1.1版本的套接字

	err = WSAStartup(wVersionRequested, &wsaData);
	if (err != 0)
	{
		return;
	}//加载套接字库，加裁失败则返回
	if (LOBYTE(wsaData.wVersion) != 1 || HIBYTE(wsaData.wVersion) != 1)
	{
		WSACleanup();
		return;
	}//如果不是1.1的则退出
	SOCKET sockServer = socket(AF_INET, SOCK_STREAM, 0);
	SOCKADDR_IN addrServer;//地址结构信息
	addrServer.sin_addr.S_un.S_addr = htonl(INADDR_ANY);//IP信息，一般设置为这个就可以
	addrServer.sin_family = AF_INET;
	addrServer.sin_port = htons(listenPort);//端口
	::bind(sockServer, (SOCKADDR *)&addrServer, sizeof(SOCKADDR));//绑定
	listen(sockServer, 5);//监听
	SOCKADDR_IN sockClient;
	int IClientSockLen = sizeof(SOCKADDR);

	SOCKET sockConn;
	while (1)
	{
		sockConn = accept(sockServer, (SOCKADDR *)&sockClient, &IClientSockLen);//接受连接请求

		mTempSocket = sockConn;
		std::thread serverRecvThread(&TheApp::ServerRecvTCP, this);
		serverRecvThread.detach();
	}
}

void TheApp::ServerRecvTCP()
{
	SimOne_Data_Control vehControl;
	SOCKET interSockConn = mTempSocket;
	const int recvBufSize = 4096;
	char recvbuf[recvBufSize] = { 0 };
	int iResult = 0;
	string connStatusMsg = "";

	int sendCount = 0;

	while (1)
	{
		ZeroMemory(recvbuf, recvBufSize);
		iResult = recv(interSockConn, recvbuf, recvBufSize, 0);
		{
			// cout << "Recv length: " << iResult << endl;
		}
		connStatusMsg = "";

		if (iResult > 0)
		{
			if (strcmp(recvbuf, "ALLSPARK_CONNECT") == 0)
			{
				mConnNodeSocket.push_back(interSockConn);
				{
					cout << "Allspark connect OK!" << endl;
				}
			}
			else if (strcmp(recvbuf, "SIMULATOR_CONNECT") == 0)
			{
				mConnNodeSocket.push_back(interSockConn);
				{
					cout << "Simulator connect OK!" << endl;
				}
			}
			else if (strcmp(recvbuf, "BRIDGE2SIMONE") == 0)
			{
				// Drive decide function can add here
				Bridge2SimOneStruct bridge2SimOneData;
				memcpy(&bridge2SimOneData, recvbuf, sizeof(bridge2SimOneData));

				SimOne_Data_Control simoneControlData = bridge2SimOneData.vehControl;

				/*cout
					<< clock() << "	"
					<< simoneControlData.throttle << "	"
					<< simoneControlData.brake << "	"
					<< simoneControlData.steering << "	"
					<< simoneControlData.throttleMode << "	"
					<< simoneControlData.brakeMode << "	"
					<< simoneControlData.steeringMode << "	"
					<< endl;*/

				cybertron::proto::sensor::DataVehicleControlState state;

				state.set_throttlepercentage(simoneControlData.throttle);
				state.set_throttlemode((cybertron::proto::sensor::EThrottleMode)simoneControlData.throttleMode);
				state.set_steering(simoneControlData.steering);
				state.set_steeringmode((cybertron::proto::sensor::ESteeringMode)simoneControlData.steeringMode);
				state.set_brake(simoneControlData.brake);
				state.set_brakemode((cybertron::proto::sensor::EBrakeMode)simoneControlData.brakeMode);
				state.set_ismanualgear(simoneControlData.isManualGear);
				state.set_handbrake(simoneControlData.handbrake);
				// state.set_controllername(bridge2SimOneData.controllerName);
				state.set_controllername("Simulator");
				if (simoneControlData.gear <= ESimOne_Gear_Mode_Parking)
				{
					state.set_gearmode((cybertron::proto::sensor::EGearMode)simoneControlData.gear);
				}
				else
				{
					state.set_gearmode((cybertron::proto::sensor::EGearMode)(simoneControlData.gear + 46));
				}


				mDynClient.send(proto::sensor::EDataType_VehicleControlState, state);
			}
			else
			{
				
			}
		}

		if (iResult == -1)
		{
			{
				cout << clock() << " "
					<< interSockConn << " disconnect" << endl;
			}

			// Clear state when tcp disconnect
			cybertron::proto::sensor::DataVehicleControlState state;

			state.set_throttlepercentage(0);
			state.set_throttlemode(cybertron::proto::sensor::EThrottleMode::EThrottle_PERCENT);
			state.set_steering(0);
			state.set_steeringmode(cybertron::proto::sensor::ESteeringMode::ESteering_PERCENT);
			state.set_brake(0);
			state.set_brakemode(cybertron::proto::sensor::EBrakeMode::EBrake_PERCENT);
			state.set_ismanualgear(false);
			state.set_handbrake(false);
			state.set_controllername("");
			state.set_gearmode(cybertron::proto::sensor::EGearMode::EGearMode_Neutral);

			mDynClient.send(proto::sensor::EDataType_VehicleControlState, state);

			for (int connNodeIndex = 0; connNodeIndex < mConnNodeSocket.size(); connNodeIndex++)
			{
				if (mConnNodeSocket[connNodeIndex] == interSockConn)
				{
					vector<SOCKET>::iterator connNodeSocketIter = mConnNodeSocket.begin() + connNodeIndex;
					mConnNodeSocket.erase(connNodeSocketIter);

					{
						cout << mConnNodeSocket.size() << endl;
					}

					break;
				}
			}

			return;
		}
	}

	return;
}
